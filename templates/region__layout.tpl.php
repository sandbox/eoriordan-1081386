<?php if ($content): ?>
  <div id="<?php print $region_id; ?>" class="<?php print $classes; ?>">
    <div class="inner">
      <?php print $content; ?> 
    </div>
  </div>
<?php endif; ?>