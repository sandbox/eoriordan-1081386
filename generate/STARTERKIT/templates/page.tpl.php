<?php print render($page['header']); ?>

<?php if ($page['help'] || ($show_messages && $messages)): ?>
  <div id='console'>
    <?php print render($page['help']); ?>
    <?php if ($show_messages && $messages): print $messages; endif; ?>
  </div>
<?php endif; ?>

<div id='header' role='banner'>
  <?php if ($site_name): ?>
    <h1 class='site-name'><?php print $site_name ?></h1>
  <?php endif; ?>
</div>

<div id='navigation' role='navigation'>
  <?php print $main_menu_output; ?>
  <?php //print $secondary_menu_output; ?>
</div>

<div id='page' class="clearfix" role='main'>
  
  <?php print render($page['sidebar_first']) ?>
  
  <div id='main-content' class="clearfix" role='main'>
    <?php if ($breadcrumb) print $breadcrumb; ?>
    <?php if ($title): ?><h1 class='page-title'><?php print $title ?></h1><?php endif; ?>
    <?php print render($tabs); ?>
    <?php print render($action_links); ?>
    <?php print render($page['highlighted']); ?>
    <?php print render($page['content']) ?>
  </div>
  
  <?php print render($page['sidebar_second']) ?>

</div>

<?php print render($page['footer']) ?>