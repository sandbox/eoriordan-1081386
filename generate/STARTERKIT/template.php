<?php

$DEVEL = TRUE;
if($DEVEL) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();  
}

function STARTERKIT_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}

function STARTERKIT_preprocess_page(&$variables, $hook) {
  $variables['site_name'] = l($variables['site_name'], '<front>');
}

function STARTERKIT_preprocess_block(&$variables, $hook) {
  $variables['sample_variable'][] = t('Lorem ipsum.');
}

function STARTERKIT_preprocess_node(&$variables, $hook) {

  // Make the $submitted a bit nicer
  $vars['date'] = date('j M Y', $vars['created']);
  $vars['submitted'] = t('By !username | !datetime', array(
    '!username' => $vars['name'],
    '!datetime' => $vars['date'],
  ));

  // Optionally, run node-type-specific preprocess functions, like
  // STARTERKIT_preprocess_node_page() or STARTERKIT_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}

function STARTERKIT_preprocess_breadcrumb(&$variables) {
  $variables['breadcrumb_append_title'] = TRUE;
  $variables['breadcrumb_prefix'] = '';
  $variables['breadcrumb_separator'] = ' &raquo; ';
}

function STARTERKIT_form_search_block_form_alter(&$form) {
  $form['search_block_form']['#attributes']['placeholder'] = t('Search');
}


