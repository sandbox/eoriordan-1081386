screenshot  = screenshot.png
name        = Fussy Sub-theme Starter Kit
description = A theme to hack on. Based on fussy.

core        = 7.x
base theme  = fussy

stylesheets[all][] = css/screen.css
stylesheets[print][] = css/print.css
;stylesheets-conditional[IE][all][] = css/ie.css

scripts[] = javascript/init.js

regions[header]         = Header
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer]         = Footer

; The admin bar and Drupal needs the bellow regions
regions[page_top]       = Page top
regions[page_bottom]    = Page bottom
