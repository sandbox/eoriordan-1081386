<?php

include 'includes/core_theme.inc';
include 'includes/menu_theme.inc';
include 'includes/fields_theme.inc';
include 'includes/views_theme.inc';  
//include 'includes/pager_theme.inc';
include 'includes/panels_theme.inc';

$var = 'Edward';

$var = 'Edwadsafdf

function fussy_preprocess_html(&$vars) {
  // Add an identifier so we know that the theme is active
  $vars['classes_array'][] = 'fussy';
  
  // Store the menu item since it has some useful information.
  $vars['menu_item'] = menu_get_item();

  switch ($vars['menu_item']['page_callback']) {
    case 'views_page':
      // Is this a Views page?
      $vars['classes_array'][] = 'page-views';
      break;
  }
  
  $panel_page_callbacks = array(
    'page_manager_page_execute',
    'page_manager_node_view',
    'page_manager_node_view_page',
    'page_manager_contact_site'
  );
  if(in_array($vars['menu_item']['page_callback'], $panel_page_callbacks)) {
    $vars['classes_array'][] = 'page-panels';   
  } else {
    $vars['classes_array'][] = 'page-not-panels';   
  }
}

function fussy_process_page(&$vars) {
  
  // Create the main menu theme output
  $vars['main_menu_output'] = theme('links__system_main_menu', 
    array(
      'links' => $vars['main_menu'], 
      'attributes' => array(
        'class' => 'links main-menu'
      ),
      'heading' => array(
        'text' => t('Main Menu'),
        'level' => 'h2',
        'class' => array('element-invisible'),
      ),
    )
  );
  
  // Create the user/secondary menu theme output
  $vars['secondary_menu_output'] = theme('links__system_secondary_menu', 
    array(
      'links' => $vars['secondary_menu'], 
      'attributes' => array(
        'class' => 'links secondary-menu'
      ),
      // 'heading' => array(
      //   'text' => t('Secondary Menu'),
      //   'level' => 'h2',
      //   'class' => array('element-invisible'),
      // ),
    )
  );
}

/**
 * Implementation of preprocess_node().
 */
function fussy_preprocess_node(&$vars) {
  
  // Add a class to allow styling based on publish status.
  if ($vars['status']) {
    $vars['classes_array'][] = 'node-published';
  }
  // Add a class to allow styling based on promotion.
  if (!$vars['promote']) {
    $vars['classes_array'][] = 'node-not-promoted';
  }
  // Add a class to allow styling based on sticky status.
  if (!$vars['sticky']) {
    $vars['classes_array'][] = 'node-not-sticky';
  }
  // Add a class to allow styling of nodes being viewed by the author of the node in question.
  if ($vars['uid'] == $vars['user']->uid) {
    $vars['classes_array'][] = 'self-posted';
  }
  // Add a class to allow styling based on the node author.
  $vars['classes_array'][] = drupal_html_class('nodeauthor-' . $vars['node']->name);
  // Add a class based on viewmode
  $vars['classes_array'][] = 'node-viewmode-' . $vars['view_mode'];
  
  // Add classes for node content
  $vars['content_attributes_array']['class'][] = 'content';
  
  if (isset($vars['content']['links'])) {
    $vars['links'] = $vars['content']['links'];
    unset($vars['content']['links']);
  }

  if (isset($vars['content']['comments'])) {
    $vars['comments'] = $vars['content']['comments'];
    unset($vars['content']['comments']);
  }
}

/**
 * Preprocessor for preprocess_block()
 */

function fussy_preprocess_block(&$vars, $hook) {

  // Classes describing the position of the block within the region.
  if ($vars['block_id'] == 1) {
    $vars['classes_array'][] = 'first';
  }
  // The last_in_region property is set in zen_page_alter().
  if (isset($vars['block']->last_in_region)) {
    $vars['classes_array'][] = 'last';
  }
  $vars['classes_array'][] = $vars['block_zebra'];
  
  // Add a class to provide CSS for blocks without titles.
  if(empty($vars['block']->subject) && (string) $vars['block']->subject != '0') {
    $vars['classes_array'][] = 'block-without-title';
  } else {
    $vars['classes_array'][] = 'block-with-title';  
  }
  
  $vars['title_attributes_array']['class'] = 'title';
  
  // The main content block is probably not going to be styled
  // like other blocks. Remove the generic block class and add
  // something more specific
  if ($vars['block']->module === 'system' && $vars['block']->delta === 'main') {
    $vars['classes_array'] = array_diff($vars['classes_array'], array('block'));
    $vars['classes_array'][] = 'block-page-content';
  }
  
  if ($vars['block']->module == 'block' || $vars['block']->module == 'boxes') {
    $vars['content_attributes_array']['class'][] = 'prose';
  }
  $vars['content_attributes_array']['class'][] = 'content';
}

function fussy_process_block(&$variables, $hook) {
  // Drupal 7 should use a $title variable instead of $block->subject.
  $variables['title'] = $variables['block']->subject;
}

function fussy_preprocess_region(&$vars) {
  $blocks = element_children($vars['elements']);
  
  // Check to see if context flag is being used.
  // If so remove it. It is not a block
  foreach ($blocks as $key => $block) {
    if($block == 'context')
      unset($blocks[$key]);  
  }
  // Tell number of blocks in a region in a class
  $num = count($blocks);
  $vars['classes_array'][] = 'has-' . $num . '-blocks';
  
  $vars['region_id'] = drupal_html_id($vars['region']);
  
  // Sidebar regions get some extra classes and a common template suggestion.
  if (strpos($vars['region'], 'sidebar_') === 0) {
    $vars['classes_array'][] = 'sidebar';
    $vars['theme_hook_suggestions'][] = 'region__layout';
    // Allow a region-specific template to override.
    $vars['theme_hook_suggestions'][] = 'region__' . $vars['region'];
  }
}

/**
 * Implements hook_page_alter().
 * @param $page
 *   Nested array of renderable elements that make up the page.
 */
function fussy_page_alter(&$page) {
  // Look in each visible region for blocks.
  foreach (system_region_list($GLOBALS['theme'], REGIONS_VISIBLE) as $region => $name) {
    if (!empty($page[$region])) {
      // Find the last block in the region.
      $blocks = array_reverse(element_children($page[$region]));
      while ($blocks && !isset($page[$region][$blocks[0]]['#block'])) {
        array_shift($blocks);
      }
      if ($blocks) {
        $page[$region][$blocks[0]]['#block']->last_in_region = TRUE;
      }
    }
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $variables
 *   - title: An optional string to be used as a navigational heading to give
 *     context for breadcrumb links to screen-reader users.
 *   - title_attributes_array: Array of HTML attributes for the title. It is
 *     flattened into a string within the theme function.
 *   - breadcrumb: An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function fussy_preprocess_breadcrumb(&$variables) {
  $variables['breadcrumb_append_title'] = TRUE;
  $variables['breadcrumb_prefix'] = '';
  $variables['breadcrumb_separator'] = ' &raquo; ';
}
 
function fussy_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  
  // Return the breadcrumb with separators.
  if (!empty($breadcrumb)) {

    $trailing_separator = $title = '';
    if ($variables['breadcrumb_append_title']) {
      $item = menu_get_item();
      if (!empty($item['tab_parent'])) {
        // If we are on a non-default tab, use the tab's title.
        $title = check_plain($item['title']);
      }
      else {
        $title = drupal_get_title();
      }
      if ($title) {
        $trailing_separator = $variables['breadcrumb_separator'];
      }
    }

    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users.
    if (empty($variables['title'])) {
      $variables['title'] = t('You are here');
    }
    // Unless overridden by a preprocess function, make the heading invisible.
    if (!isset($variables['title_attributes_array']['class'])) {
      $variables['title_attributes_array']['class'][] = 'element-invisible';
    }
    $heading = '<h2' . drupal_attributes($variables['title_attributes_array']) . '>' . $variables['title'] . '</h2>';
    
    $output = '<div class="breadcrumb">';
    $output .= $heading;
    $output .= $variables['breadcrumb_prefix'];
    $output .= implode($variables['breadcrumb_separator'], $breadcrumb);
    $output .= '<span class="title">' . $trailing_separator . $title . '</span>';
    $output .= '</div>';
    
    return $output;    
  }
  // Otherwise, return an empty string.
  return '';
}
