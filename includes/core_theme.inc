<?php

/*
  TODO find a better way to check for an image
*/
/**
 * Add classes to links based on the content of the link
 * Add 'image-link' if content is image
 */
function fussy_link(&$variables) {
  if($variables['options']['html']) {
    if( strpos($variables['text'], '<img') !== false  ) {
      $variables['options']['attributes']['class'][] = 'image-link'; 
    }
  }
    
  return '<a href="' . check_plain(url($variables['path'], $variables['options'])) . '"' . drupal_attributes($variables['options']['attributes']) . '>' . ($variables['options']['html'] ? $variables['text'] : check_plain($variables['text'])) . '</a>';
}

/**
 * Change feed icon to be text rather than an image.
 * Easier to swap in whatever image we want in theme layer.
 */
function fussy_feed_icon($variables) {
  $text = t('Subscribe to @feed-title', array('@feed-title' => $variables['title']));
  
  return l(
    $text, $variables['url'], array(
      'attributes' => array('class' => array('feed-icon'))
    )
  );
}