<?php 

function fussy_field($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h5 class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</h5>';
  }

  // Render the items.
  if($variables['multiple-items']) {
    $output .= '<ul class="field-items"' . $variables['content_attributes'] . '>';
    foreach ($variables['items'] as $delta => $item) {
      $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
      $output .= '<li class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
    }
    $output .= '</ul>';  
  } else {
    foreach ($variables['items'] as $delta => $item) {
      $output .= drupal_render($item);  
    }
  }

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function fussy_preprocess_field(&$variables, $hook) { 
  $variables['multiple-items'] = FALSE;
  if(count($variables['items']) > 1) {
    $variables['multiple-items'] = TRUE;
  }
  
  // Add prose class to long text fields.
  if ($variables['element']['#field_type'] === 'text_with_summary') {
    $variables['classes_array'][] = 'prose';
  }
}


