<?php

function fussy_panels_default_style_render_region($vars) {
  
  $output = '<div class="has-' . count($vars['panes']) . '-items">';
  foreach ($vars['panes'] as $pane_id => $pane_output) {
    $output .= $pane_output;
  }
  $output .= '</div>';
  return $output;
}

function fussy_preprocess_panels_pane(&$vars) {
  $content = &$vars['content'];
  $pane = &$vars['pane']; 
  
  // Add pane number for browsers withtout pseudo selectors
  $vars['classes_array'][] = 'pane-num-' . ($pane ->position+1);
  
  // You don't need the full panels-pane.tpl.php for some panes.
  // Give a way to skip most of the markup in the template
  $vars['render_fully'] = TRUE;
  if($pane->type == 'entity_field') {
    $vars['render_fully'] = FALSE;
  }
}

/*
  Add 'pane-list' class
*/
function fussy_panels_list_style_render_region($vars) {
  $display = $vars['display'];
  $region_id = $vars['region_id'];
  $panes = $vars['panes'];
  $settings = $vars['settings'];

  $items = array();

  foreach ($panes as $pane_id => $item) {
    $items[] = $item;
  }

  if (empty($settings['list_type'])) {
    $settings['list_type'] = 'ul';
  }

  $attributes = array('class' => 'pane-list items-' . count($panes));
  return theme('item_list', array('items' => $items, 'type' => $settings['list_type'], 'attributes' => $attributes));
}