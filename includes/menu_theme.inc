<?php

/**
 * Implements theme_menu_local_task().
 */
function fussy_preprocess_menu_local_task(&$variables) {
  $link_class = drupal_html_class($variables['element']['#link']['title']);
  
  $variables['element']['#link']['localized_options']['attributes'] = array();
  $variables['element']['#link']['localized_options']['attributes']['class'] = array($link_class);
}