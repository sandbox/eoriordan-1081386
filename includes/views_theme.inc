<?php

// Add class for total number of rows
// Add class based on whether view has or does not have header
function fussy_preprocess_views_view(&$vars) {
  
  if (!empty($vars['view']->style_options['grouping']) ) {
    $vars['classes_array'][] = 'views-grouping-list';
  }
  
  if (!empty($vars['header'])) {
    $vars['classes_array'][] = 'with-header';
  }
  if (!empty($vars['footer'])) {
    $vars['classes_array'][] = 'with-footer';
  }
  // Give a class for the number of rows the view will have
  $vars['classes_array'][] = 'has-' . count($vars['view']->result) . '-rows';
  
  // Give class based on view handeler (i.e. view-display-page)
  $display_class = str_replace("plugin_", "", get_class( $vars['view']->display_handler ));
  $vars['classes_array'][] = drupal_html_class($display_class);
  
  $vars['classes_array'][] = 'base-table-' . $vars['view']->base_table;
  
  // If we are using Panels views rows detect that
  if (isset($vars['view']->style_plugin->row_plugin->definition['handler'])) {
    if($vars['view']->style_plugin->row_plugin->definition['handler'] == 'panels_views_plugin_row_fields') {
      $vars['classes_array'][] = 'views-panel'; 
    }
  }
}

// Add a 'views-list' class to the ol, ul
// Remove the wrapper <div class='item-list'/>
function fussy_preprocess_views_view_list(&$vars) {
  $handler  = $vars['view']->style_plugin;
  
  // Add view-list class
  $vars['list_type_prefix'] = '<' . $handler->options['type'] . ' class="views-list">';
  
  // If class has been set use it and views-list
  if ($vars['class']) {
    $vars['list_type_prefix'] = '<' . $handler->options['type'] . ' class="' . $vars['class'] . ' views-list">';
  }
}






